# HelloWorlds

## Introduction
Hi, my name is Rob de Hoon. I am a programmer from The Netherlands and program mostly in C, C++ and PHP. I started this repo to learn how to use GitLab.

## HelloWorlds
As stated before: I am trying to learn how to use GitLab. Now, I could have simply written _Hello World_ in this README-file. But I also wanted to test out how to work with pulling, pushing and merge requests. And work from different platforms.
